//
//  PackageDescription.swift
//  xspmkit
//
import Foundation

struct PackageDescription: Codable, Equatable {
    struct PackageTarget: Codable, Equatable {
        let c99Name: String
        let moduleType: String
        let name: String
        let path: String
        let sources: [String]
        let type: String
        
        enum CodingKeys: String, CodingKey {
            case c99Name = "c99name"
            case moduleType = "module_type"
            case name, path, sources, type
        }
    }
        
    let name: String
    let path: String
    let targets: [PackageTarget]
    
    init(fromTreeData data: Data) throws {
        let decoder = JSONDecoder()
        self = try decoder.decode(PackageDescription.self, from: data)
    }
}

