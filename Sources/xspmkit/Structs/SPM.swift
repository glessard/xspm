//
//  SPM.swift
//  xspmkit
//

import Foundation
import PathKit

public struct SPM {
    private init() {} 
    
    public enum Command: String {
        case clean
        case describe
        case edit
        case fetch
        case `init`
        case reset
        case resolve
        case unedit
        case update
        case dumpPackage = "dump-package"
        case generateXcodeproj = "generate-xcodeproj"
        case showDependencies = "show-dependencies"
        case toolsVersion = "tools-version"
    }
    
    @discardableResult public static func perform(command: Command, printOutput: Bool, saveSuccessLog flag: Bool, extraArgs: [String] = [], inRoot root: Path) throws -> Data {
        return try SPM.perform(command: command.rawValue, printOutput: printOutput, saveSuccessLog: flag, extraArgs: extraArgs, inRoot: root)
    }
    
    @discardableResult private static func perform(command: String, printOutput: Bool, saveSuccessLog flag: Bool, extraArgs: [String], inRoot root: Path) throws -> Data {
        let task = Process()
        let taskStdout = Pipe()
        let taskStderr = Pipe()
        
        var stdoutData = Data()
        
        if (printOutput) {
            taskStdout.fileHandleForReading.readabilityHandler = { (fh) in
                let data = fh.readDataToEndOfFile()
                guard data.count > 0 else { return }
                let line = String(data: data, encoding: .utf8)
                stdoutData += data
                if let message = line { print(message) }
            }
        }
        
        task.standardOutput = taskStdout
        task.standardError = taskStderr
        task.launchPath = "/usr/bin/env"
        task.arguments = ["swift", "package", command] + extraArgs
        task.currentDirectoryURL = root.url
        task.qualityOfService = .userInitiated
        task.launch()
        task.waitUntilExit()
        
        guard task.terminationStatus == 0 else {
            let logData = taskStderr.fileHandleForReading.readDataToEndOfFile()
            let logName = try SPM.write(logData: logData, labeled: "error")
            throw XSPMError.swiftPMError(logName)
        }
        
        if flag {
            let logData = taskStdout.fileHandleForReading.readDataToEndOfFile()
            let logName = try SPM.write(logData: logData, labeled: "success")
            LoggerManager.logInfo("spm log written to: \(logName)")
        }
        
        return printOutput ? stdoutData : taskStdout.fileHandleForReading.readDataToEndOfFile()
    }
    
    @discardableResult
    private static func write(logData data: Data, labeled label: String, inRoot root: Path = Path.current) throws -> String {
        let timestamp = Date().timeIntervalSince1970
        let logName = "spm_\(label)_\(timestamp).log"
        let logPath = root + logName
        try logPath.write(data)
        return logName
    }
}
