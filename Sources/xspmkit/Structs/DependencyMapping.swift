//
//  DependencyMapping.swift
//  xspmkit
//

import Foundation

public struct DependencyMapping: Codable, Equatable {
    public let target: String
    public let products: [String]
}

extension DependencyMapping: CustomStringConvertible {
    public var description: String {
        if self.products.isEmpty {
            return "\(self.target): none"
        } else {
            let productList = self.products.joined(separator: ", ")
            return "\(self.target): \(productList)"
        }
    }
}

extension DependencyMapping: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.target)
    }
}

extension Array where Element == DependencyMapping {
    var targets: [String] { return self.map { $0.target } }
    
    func diff(with rhs: [DependencyMapping], against tree: ResolvedDependenciesTree) -> DependencyMappingDiffResult {
        LoggerManager.logDebug("Starting dependency tree comparison")
        let lhtargets = self.targets.set
        let rhtargets = rhs.targets.set
        
        LoggerManager.logDebug("Number of target then: \(rhs.targets.count)")
        LoggerManager.logDebug("Number of target now: \(self.targets.count)")
        
        let commonTargets = lhtargets.intersection(rhtargets)
        LoggerManager.logDebug("Number of common targets: \(commonTargets.count)")
        let removedTargets = lhtargets.subtracting(commonTargets)
        LoggerManager.logDebug("Number of removed targets: \(removedTargets.count)")
        let newTargets = rhtargets.subtracting(commonTargets)
        LoggerManager.logDebug("Number of new targets: \(newTargets.count)")
        var unchangedTargets: [String] = []
        
        let updatedTargets = commonTargets.compactMap { (target) -> DependencyMappingDiffResult.TargetDiffResult? in
            guard let lhdep = self[target], let rhdep = rhs[target] else { return nil }
            let lhproducts = lhdep.products.set
            let rhproducts = rhdep.products.set
            
            let commonProducts = lhproducts.intersection(rhproducts)
            let newProducts = rhproducts.subtracting(commonProducts)
            let removedProducts = lhproducts.subtracting(commonProducts)
            let obsoleteProducts = removedProducts.compactMap { tree.packageExists($0) ? $0 : nil }
            
            guard !newProducts.isEmpty && !removedProducts.isEmpty && !obsoleteProducts.isEmpty else {
                unchangedTargets.append(target)
                return nil
            }
            
            return DependencyMappingDiffResult.TargetDiffResult(name: target, newProducts: newProducts.array, obsoleteProduts: obsoleteProducts, removedProducts: removedProducts.array, unchangedProducts: commonProducts.array)
        }
        
        
        
        return DependencyMappingDiffResult(removedTargets: removedTargets.array, newTargets: newTargets.array, updatedTargets: updatedTargets, unchangedTargets: unchangedTargets)
    }
    
    public subscript(target: String) -> DependencyMapping? {
        return self.first(where: { $0.target == target })
    }
}

public struct DependencyMappingDiffResult {
    public struct TargetDiffResult {
        public let name: String
        public let newProducts: [String]
        public let obsoleteProduts: [String]
        public let removedProducts: [String]
        public let unchangedProducts: [String]
    }
    
    public let removedTargets: [String]
    public let newTargets: [String]
    public let updatedTargets: [TargetDiffResult]
    public let unchangedTargets: [String]
    
    public var isUseless: Bool {
        return self.removedTargets.count + self.newTargets.count + self.updatedTargets.count + self.unchangedTargets.count == 0
    }
}

