//
//  SequenceExtension.swift
//  xspmkit
//

public extension Sequence {
    func wholeMap<T>(_ transform: (Self) throws -> T) rethrows -> T {
        return try transform(self)
    }
}
