//
//  Parser.swift
//  xspmkit
//

import Foundation
import Result

class Parser {
    private enum VersionMode {
        case from(String)
        case exact
        case branch(String)
        case revision(String)
        case upToNextMajor
        case upToNextMinor
        case openRange
        case closedRange
        case master
    }
    
    private enum SeekingMode {
        case idle
        case indentifier
        case value
        case op
        case whiteSpace
        case url
        case productsStart
        case delimiter
        case products
        case version
        case semver
        case semverRangeLower
        case semverRangeUpper
        case semverRangeDelimiter
        case semverRangeEnder
        case target
        case colon
        case eol
    }
    
    enum Value {
        case text(String)
        case integer(Int)
        case float(Double)
        case bool(Bool)
        
        var asString: String? {
            return self.untypedValue as? String
        }
        
        var asInt: Int? {
            return self.untypedValue as? Int
        }
        
        var asDouble: Double? {
            return self.untypedValue as? Double
        }
        
        var asBool: Bool? {
            return self.untypedValue as? Bool
        }
        
        var untypedValue: Any {
            switch self {
            case .text(let value as Any), .float(let value as Any), .integer(let value as Any), .bool(let value as Any):
                return value
            }
        }
    }
    
    private let tokens: [Token]
    private var cursor: Int
    private var lastNoSpaceCursor: Int?
    private var spacePrecedes: Bool
    private var currentToken: Token {
        return self.tokens[self.cursor]
    }
    private var previous: Token {
        let previousCursor = self.cursor.advanced(by: -1)
        return previousCursor >= 0 ? self.tokens[previousCursor] : .none
    }
    private var lastNonSpaceToken: Token {
        guard let previousCursor = self.lastNoSpaceCursor else { return .none }
        return self.tokens[previousCursor]
    }
    
    private var isEOF: Bool {
        if case .eof = self.currentToken {
            return true
        } else {
            return false
        }
    }
    private var isEOL: Bool {
        if case .eol = self.currentToken {
            return true
        } else {
            return false
        }
    }
    
    
    private var properties: [ManifestContent.PropertyKey: Value]
    private var dependencies: [Dependency]
    private var dependencyMappings: [DependencyMapping]
    
    init(tokens: [Token]) {
        self.tokens = tokens
        self.cursor = 0
        self.properties = [:]
        self.dependencies = []
        self.dependencyMappings = []
        self.spacePrecedes = false
    }
    
    private func consume() {
        guard self.cursor + 1 < self.tokens.endIndex else { return }
        self.cursor = self.cursor.advanced(by: 1)
        if case .whiteSpace = self.currentToken {
            self.consume()
        } else {
            self.lastNoSpaceCursor = self.cursor
            if case .whiteSpace = self.previous {
                self.spacePrecedes = true
            } else {
                self.spacePrecedes = false
            }
        }
    }
    
    
    /// Parse the token list and return the corresponding manifest content.
    ///
    /// - Returns: The manifest content.
    /// - Throws: Any `ParserError`
    func parse() throws -> ManifestContent {
        try self.parseTokenList()
        return try self.generateManifestContent()
    }
    
    func validate() -> Result<Bool, ParserError> {
        let mirror = self
        do {
            try mirror.parseTokenList()
            return Result(value: true)
        } catch let error as ParserError {
            return Result(error: error)
        } catch {
            return Result(value: false)
        }
    }
    
    private func parseTokenList() throws {
        repeat {
            switch self.currentToken {
            case .kind(let data):
                switch data.buffer {
                case "+":
                    try self.parseDependencyLine()
                case "-":
                    try self.parsePropertyLine()
                case "#":
                    try self.parseDependencyMappingLine()
                default:
                    throw ParserError.unexpectedToken(position: data.start.position, text: data.buffer)
                }
            case .whiteSpace, .eol, .eof:
                self.consume()
                break
            default:
                if let data = self.currentToken.data {
                    throw ParserError.unexpectedToken(position: data.start.position, text: data.buffer)
                } else {
                    throw ParserError.unknown
                }
            }
        } while !self.isEOF
    }
    
    private func generateManifestContent() throws -> ManifestContent {
        guard let projectValue =  self.properties[ManifestContent.PropertyKey.projectName],
            case Value.text(let projectName) = projectValue else { throw ParserError.missingProjectName }
        
        return ManifestContent(projectName: projectName, properties: self.properties, dependencies: self.dependencies, mappings: self.dependencyMappings)
    }
    
    private func parsePropertyLine() throws {
        var mode: SeekingMode = .indentifier
        var identifier: ManifestContent.PropertyKey = ""
        var value: Value!
        repeat {
            self.consume()
            switch mode {
            case .indentifier:
                switch self.currentToken {
                case .identifier(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    identifier = data.buffer
                    mode = .colon
                case .colon(_), .eol, .eof:
                    try self.throwExpected("property identifier")
                default:
                    try self.throwUnexpected()
                }
            case .colon:
                switch self.currentToken {
                case .colon(_):
                    mode = .value
                case .eol, .eof:
                    try self.throwExpected("colon (:)")
                default:
                    try self.throwUnexpected()
                }
            case .value:
                switch self.currentToken {
                case .string(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    value = Value.text(data.buffer.removingFirstAndLast())
                    mode = .eol
                case .number(let data, let floating):
                    self.warnNoSpaceIfNeeded(data: data)
                    if floating {
                        guard let number = Double(data.buffer) else { throw ParserError.unknown }
                        value = .float(number)
                    } else {
                        guard let number = Int(data.buffer) else { throw ParserError.unknown }
                        value = .integer(number)
                    }
                    mode = .eol
                case .bool(let data, let bool):
                    self.warnNoSpaceIfNeeded(data: data)
                    value = .bool(bool)
                    mode = .eol
                case .eol, .eof:
                    try self.throwExpected("value")
                default:
                    try self.throwUnexpected()
                }
            default:
                break
            }
        } while !(self.isEOL || self.isEOF)
        
        if !identifier.isValid {
            LoggerManager.logDebug("Manifest: key \(identifier) is not a valid key.")
        }
        
        guard self.properties[identifier] == nil else { throw ParserError.duplicateProperty(name: identifier) }
        self.properties[identifier] = value
        
        if !self.isEOF { self.consume() }
    }
    
    private func parseDependencyLine() throws {
        var mode: SeekingMode = .url
        var url: String!
        var products: [String] = []
        var version = VersionMode.master
        var semver: String!
        var semverLowerBound: String!
        var semverUpperBound: String!
        
        repeat {
            self.consume()
            
            switch mode {
            case .url:
                switch self.currentToken {
                case .string(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    url = data.buffer.removingFirstAndLast()
                    mode = .productsStart
                case .identifier(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    url = data.buffer
                    mode = .productsStart
                case .eol, .eof:
                    try self.throwExpected("repositoty URL")
                default:
                    try self.throwUnexpected()
                }
            case .productsStart:
                switch self.currentToken {
                case .openbracket(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    mode = .products
                case .string(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    products.append(data.buffer.removingFirstAndLast())
                    mode = .version
                case .identifier(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    products.append(data.buffer)
                    mode = .version
                case .eol, .eof:
                    try self.throwExpected("product list")
                default:
                    try self.throwUnexpected()
                }
            case .products:
                switch self.currentToken {
                case .string(let data):
                    products.append(data.buffer.removingFirstAndLast())
                    mode = .delimiter
                case .identifier(let data):
                    products.append(data.buffer)
                    mode = .delimiter
                case .eol, .eof:
                    try self.throwExpected("product name")
                default:
                    try self.throwUnexpected()
                }
            case .delimiter:
                switch self.currentToken {
                case .comma(_):
                    mode = .products
                case .closebracket(let data):
                    if case .comma(_) = self.lastNonSpaceToken {
                        try self.throwUnexpected()
                    } else if products.isEmpty {
                        throw ParserError.syntaxError(position: data.end.position, reason: "empty produduct list")
                    } else {
                        mode = .version
                    }
                case .eol, .eof:
                    try self.throwExpected("product list delimiter")
                default:
                    try self.throwUnexpected()
                }
            case .version:
                switch self.currentToken {
                case .eol, .eof:
                    break
                case .op(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    switch data.buffer {
                    case "==":
                        version = .exact
                    case ">=":
                        version = .upToNextMajor
                    case ">~":
                        version = .upToNextMinor
                    default:
                        throw ParserError.unknown
                    }
                    mode = .semver
                case .string(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    version = .branch(data.buffer.removingFirstAndLast())
                    mode = .eol
                case .identifier(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    version = .branch(data.buffer)
                    mode = .eol
                case .semver(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    version = .from(data.buffer)
                    mode = .eol
                case .revision(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    version = .revision(data.buffer.removingFirstAndLast())
                    mode = .eol
                case .openbracket(_):
                    mode = .semverRangeLower
                default:
                    try throwUnexpected()
                }
            case .semver:
                switch self.currentToken {
                case .semver(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    semver = data.buffer
                    mode = .eol
                case .eol, .eof:
                    try self.throwExpected("version identifier")
                default:
                    try throwUnexpected()
                }
            case .semverRangeLower:
                switch self.currentToken {
                case .semver(let data):
                    semverLowerBound = data.buffer
                    mode = .semverRangeDelimiter
                case .eof, .eol:
                    try self.throwExpected("semver range literal")
                default:
                    try self.throwUnexpected()
                }
            case .semverRangeDelimiter:
                switch self.currentToken {
                case .comma(_):
                    mode = .semverRangeUpper
                case .eof, .eol:
                    try self.throwExpected("semver range literal")
                default:
                    try self.throwUnexpected()
                }
            case .semverRangeUpper:
                switch self.currentToken {
                case .semver(let data):
                    semverUpperBound = data.buffer
                    mode = .semverRangeEnder
                case .eof, .eol:
                    try self.throwExpected("semver range literal")
                default:
                    try self.throwUnexpected()
                }
            case .semverRangeEnder:
                switch self.currentToken {
                case .openbracket(_):
                    version = .openRange
                    mode = .eol
                case .closebracket(_):
                    version = .closedRange
                    mode = .eol
                case .eof, .eol:
                    try self.throwExpected("semver range literal")
                default:
                    try self.throwUnexpected()
                }
            case .eol:
                switch self.currentToken {
                case .eol, .eof:
                    break
                default:
                    try throwUnexpected()
                }
            default:
                break
            }
        } while !(self.isEOL || self.isEOF)
        
        let dependency: Dependency
        
        switch version {
        case .from(let ver):
            dependency = .init(products: products, url: url, version: .from(ver))
        case .exact:
            dependency = .init(products: products, url: url, version: .exact(semver))
        case .branch(let branch):
            dependency = .init(products: products, url: url, version: .branch(branch))
        case .revision(let revision):
            dependency = .init(products: products, url: url, version: .revision(revision))
        case .upToNextMajor:
            dependency = .init(products: products, url: url, version: .upToNextMajor(semver))
        case .upToNextMinor:
            dependency = .init(products: products, url: url, version: .upToNextMinor(semver))
        case .openRange:
            dependency = .init(products: products, url: url, version: .openRange(.init(value1: semverLowerBound, value2: semverUpperBound)))
        case .closedRange:
            dependency = .init(products: products, url: url, version: .closedRange(.init(value1: semverLowerBound, value2: semverUpperBound)))
        case .master:
            dependency = .init(products: products, url: url, version: .branch("master"))
        }
        
        self.dependencies.append(dependency)
        
        if !self.isEOF { self.consume() }
    }
    
    private func parseDependencyMappingLine() throws {
        var mode: SeekingMode = .target
        var target: String!
        var products: [String] = []
        
        repeat {
            self.consume()
            
            switch mode {
            case .target:
                switch self.currentToken {
                case .string(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    target = data.buffer.removingFirstAndLast()
                    mode = .productsStart
                case .identifier(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    target = data.buffer
                    mode = .colon
                case .colon(_), .eol, .eof:
                    try self.throwExpected("Target name")
                default:
                    try self.throwUnexpected()
                }
            case .colon:
                switch self.currentToken {
                case .colon(_):
                    mode = .productsStart
                default:
                    try self.throwExpected("colon (:)")
                }
            case .productsStart:
                switch self.currentToken {
                case .openbracket(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    mode = .products
                case .string(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    products.append(data.buffer.removingFirstAndLast())
                    mode = .eol
                case .identifier(let data):
                    self.warnNoSpaceIfNeeded(data: data)
                    products.append(data.buffer)
                    mode = .eol
                case .eol, .eof:
                    try self.throwExpected("product list")
                default:
                    try self.throwUnexpected()
                }
            case .products:
                switch self.currentToken {
                case .string(let data):
                    products.append(data.buffer.removingFirstAndLast())
                    mode = .delimiter
                case .identifier(let data):
                    products.append(data.buffer)
                    mode = .delimiter
                case .closebracket(_):
                    if case .comma(_) = self.lastNonSpaceToken {
                        try self.throwUnexpected()
                    } else {
                        mode = .eol
                    }
                case .eol, .eof:
                    try self.throwExpected("product name")
                default:
                    try self.throwUnexpected()
                }
            case .delimiter:
                switch self.currentToken {
                case .comma(_):
                    mode = .products
                case .closebracket(_):
                    if case .comma(_) = self.lastNonSpaceToken {
                        try self.throwUnexpected()
                    } else {
                        mode = .eol
                    }
                case .eol, .eof:
                    try self.throwExpected("product list delimiter")
                default:
                    try self.throwUnexpected()
                }
            case .eol:
                switch self.currentToken {
                case .eol, .eof:
                    break
                default:
                    try throwUnexpected()
                }
            default:
                break
            }
        } while !(self.isEOL || self.isEOF)
        
        let mapping = DependencyMapping(target: target, products: products)
        guard !self.dependencyMappings.contains(where: { $0.target == target }) else {
            throw ParserError.duplicateMapping(name: target)
        }
        
        self.dependencyMappings.append(mapping)
        
        if !self.isEOF { self.consume() }
    }
    
    private func throwUnexpected() throws -> Never {
        guard let data = self.currentToken.data else {
            throw ParserError.unknown
        }
        throw ParserError.unexpectedToken(position: data.start.position, text: data.buffer)
    }
    
    private func throwExpected(_ type: String) throws -> Never {
        guard let data = self.lastNonSpaceToken.data else {
            throw ParserError.unknown
        }
        throw ParserError.expected(position: data.end.position, type: type)
    }
    
    
    private func warnNoSpaceIfNeeded(data: TokenData) {
        if !self.spacePrecedes && !self.lastNonSpaceToken.isSpecialDelimiter {
            LoggerManager.logDebug("position (\(data.start.line), \(data.start.column)): Bast-practice: expected space between two tokens.")
        }
    }
}
