//
//  LogLevel.swift
//  xspmkit
//

public enum LogLevel: Int {
    case error
    case message
    case success
    case info
    case debug
    
    func isAtLeat(level: LogLevel) -> Bool {
        return self.rawValue >= level.rawValue
    }
}
