//
//  File.swift
//  xspmkit
//

import Foundation

public enum IntegrationStatus {
    case unresolved
    case problematic
    case ok
}
