//
//  VersionConstraint.swift
//  xspmkit
//

enum Version {
    typealias StringValuePair = ValuePair<String>
    
    struct ValuePair<T: Codable&Equatable>: Codable, Equatable {
        var value1: T
        var value2: T
    }
    
    case from(String)
    case exact(String)
    case branch(String)
    case revision(String)
    case upToNextMajor(String)
    case upToNextMinor(String)
    case openRange(StringValuePair)
    case closedRange(StringValuePair)
}

extension Version: Equatable {
    static func == (lhs: Version, rhs: Version) -> Bool {
        switch (lhs, rhs) {
        case (.from(let l), .from(let r)):
            return l == r
        case (.exact(let l), .exact(let r)):
            return l == r
        case (.branch(let l), .branch(let r)):
            return l == r
        case (.revision(let l), .revision(let r)):
            return l == r
        case (.upToNextMajor(let l), .upToNextMajor(let r)):
            return l == r
        case (.upToNextMinor(let l), .upToNextMinor(let r)):
            return l == r
        case (.openRange(let l), .openRange(let r)):
            return l == r
        case (.closedRange(let l), .closedRange(let r)):
            return l == r
        default:
            return false
        }
    }
}

extension Version: CustomStringConvertible {
    var description: String {
        switch self {
        case .from(let version):
            return "from \(version)"
        case .exact(let version):
            return "exact \(version)"
        case .branch(let branch):
            return "branch \(branch)"
        case .revision(let revision):
            return "revision \(revision)"
        case .upToNextMajor(let version):
            return "upToNextMajor \(version)"
        case .upToNextMinor(let version):
            return "upToNextMinor \(version)"
        case .openRange(let pair):
            return "form \(pair.value1) to \(pair.value2) excluded"
        case .closedRange(let pair):
            return "form \(pair.value1) to \(pair.value2) included"
        }
    }
}

extension Version: SPMManifestSerializable {
    var serialized: String {
        switch self {
        case .from(let version):
            return "from: \(version.quoted)"
        case .exact(let version):
            return ".exact(\(version.quoted))"
        case .branch(let branch):
            return ".branch(\(branch.quoted))"
        case .revision(let revision):
            return ".revision(\(revision.quoted))"
        case .upToNextMajor(let version):
            return ".upToNextMajor(from: \(version.quoted))"
        case .upToNextMinor(let version):
            return ".upToNextMinor(from: \(version.quoted))"
        case .openRange(let pair):
            return "\(pair.value1.quoted)..<\(pair.value2.quoted)"
        case .closedRange(let pair):
            return "\(pair.value1.quoted)...\(pair.value2.quoted)"
        }
    }
}

extension Version: Encodable {
    private enum CodingKeys: String, CodingKey, CaseIterable {
        case from
        case exact
        case branch
        case revision
        case upToNextMajor
        case upToNextMinor
        case openRange
        case closedRange
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        switch self {
        case .from(let value):
            try container.encode(value, forKey: .from)
        case .exact(let value):
            try container.encode(value, forKey: .exact)
        case .branch(let value):
            try container.encode(value, forKey: .branch)
        case .revision(let value):
            try container.encode(value, forKey: .revision)
        case .upToNextMajor(let value):
            try container.encode(value, forKey: .upToNextMajor)
        case .upToNextMinor(let value):
            try container.encode(value, forKey: .upToNextMinor)
        case .openRange(let value):
            try container.encode(value, forKey: .openRange)
        case .closedRange(let value):
            try container.encode(value, forKey: .closedRange)
        }
    }
}

extension Version: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        var donotThrow = true
        
        self = .branch("master")
        
        for key in CodingKeys.allCases {
            do {
                switch key {
                case .from:
                    let value = try container.decode(String.self, forKey: .from)
                    self = .from(value)
                    return
                case .exact:
                    let value = try container.decode(String.self, forKey: .exact)
                    self = .exact(value)
                    return
                case .branch:
                    let value = try container.decode(String.self, forKey: .branch)
                    self = .branch(value)
                    return
                case .revision:
                    let value = try container.decode(String.self, forKey: .revision)
                    self = .revision(value)
                    return
                case .upToNextMajor:
                    let value = try container.decode(String.self, forKey: .upToNextMajor)
                    self = .upToNextMajor(value)
                    return
                case .upToNextMinor:
                    let value = try container.decode(String.self, forKey: .upToNextMinor)
                    self = .upToNextMinor(value)
                    return
                case .openRange:
                    let value = try container.decode(StringValuePair.self, forKey: .openRange)
                    self = .openRange(value)
                    return
                case .closedRange:
                    donotThrow.toggle()
                    let value = try container.decode(StringValuePair.self, forKey: .openRange)
                    self = .openRange(value)
                    return
                }
            } catch {
                guard donotThrow else { throw error }
                continue
            }
        }
    }
}
