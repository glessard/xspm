//
//  Logger.swift
//  xspmkit
//

import Foundation

public class LoggerManager {
    public typealias AnyLogger = AnyObject&LoggerProtocol
    
    private static let manager: LoggerManager = {
        return LoggerManager()
    }()
    
    private var logLevel: LogLevel
    private weak var logger: AnyLogger?
    
    private init() { self.logLevel = .message }
    
    public static func set(logLevel level: LogLevel) {
        manager.logLevel = level
    }
    
    public static func set(logger: AnyLogger) {
        manager.logger = logger
    }

    public static func logMessage(_ msg: String) {
        guard let logger = manager.logger else { return }
        if manager.logLevel.isAtLeat(level: .message) {
            logger.logMessage(msg)
        }
    }
    
    public static func logInfo(_ nfo: String) {
        guard let logger = manager.logger else { return }
        if manager.logLevel.isAtLeat(level: .info) {
            logger.logInfo(nfo)
        }
    }
    
    public static func logDebug(_ dbg: String) {
        guard let logger = manager.logger else { return }
        if manager.logLevel.isAtLeat(level: .debug) {
            logger.logDebug(dbg)
        }
    }
    
    public static func logError(_ err: String) {
        guard let logger = manager.logger else { return }
        if manager.logLevel.isAtLeat(level: .error) {
            logger.logError(err)
        }
    }
}
