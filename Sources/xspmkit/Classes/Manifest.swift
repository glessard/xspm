//
//  Manifest.swift
//  xspmkit
//

import Foundation
import PathKit
import ConvenientCharacterSets

public class Manifest {
    public static let manifestFileName = "manifest.xmnfst"
    public static let resolvedFileName = "manifest.xrslvd"
    
    private let root: Path
    
    var content: ManifestContent

    private var manifestPath: Path {
        return self.root + Manifest.manifestFileName
    }
    private var resolvedManifestPath: Path {
        return self.root + Manifest.resolvedFileName
    }
    
    private var resolvedManifest: ResolvedManifest?

    public var status: IntegrationStatus {
        var statusMask = 0
        
        statusMask += self.resolvedManifestPath.exists ? 0b0001 : 0
        statusMask += self.dependencyFolderPath.exists ? 0b0010 : 0
        statusMask += (self.dependencyFolderPath + "Package.swift").exists ? 0b0100 : 0
        statusMask += self.dependencyProjectPath.exists ? 0b1000 : 0
        
        switch statusMask {
        case 0:
            return .unresolved
        case 0b1111:
            return .ok
        default:
            return .problematic
        }
    }
    public var targetProjectName: String { return self.content.targetProjectName }
    public var targetProjectPath: Path { return self.root + self.content.targetProjectName }
    public var dependencyFolderName: String { return self.content.dependencyFolderName }
    public var dependencyFolderPath: Path { return self.root + self.dependencyFolderName }
    public var dependencyProjectPath: Path {
        return self.dependencyFolderPath + "\(self.dependencyFolderName).xcodeproj"
    }
    public var dependencies: [Dependency] { return self.content.dependencies }
    public var dependencyMappings: [DependencyMapping] { return self.content.dependenciesMappings }
    public var embedDependencyProject: Bool { return self.content.embedDependenciesProject }

    public private(set) var actualDependencyMapping: [DependencyMapping]?
    
    private init(root: Path) throws {
        self.root = root
        let manifestPath = self.root + Manifest.manifestFileName
        guard manifestPath.exists else { throw XSPMError.noManifest }
        let manifestString: String = try manifestPath.read()
        self.content = try ManifestParser(manifest: manifestString).parseManifestContent()
        if self.resolvedManifestPath.exists {
            try self.readResolved()
        }
    }
    
    public static func read(inRoot root: Path) throws -> Manifest {
        do {
            let manifest = try Manifest.init(root: root)
            return manifest
        } catch let error as ParserError {
            error.logError()
            throw XSPMError.invalidManifest
        }
    }
    
    public func getResolvedDependenciesMapping() throws -> [DependencyMapping] {
        if self.resolvedManifest == nil { try self.readResolved() }
        return self.resolvedManifest!.actualDependencyMapping
    }
    
    public func getSPMDependenciesTree() throws -> ResolvedDependenciesTree {
        if self.resolvedManifest == nil { try self.readResolved() }
        return self.resolvedManifest!.dependenciesTree
    }
    
    public func resolve(against tree: ResolvedDependenciesTree) throws -> DependencyMappingDiffResult {
        let oldDependencyMapping: [DependencyMapping] = (try? self.getResolvedDependenciesMapping()) ?? []
        
        self.actualDependencyMapping = self.dependencyMappings.map { mapping -> DependencyMapping in
            let products = mapping.products.reduce(into: Set<String>(), {
                $0.formUnion(tree.dependencies(for: $1))
            }).array
            
            return DependencyMapping(target: mapping.target, products: products)
        }
        
        let allProducts = dependencies.reduce(into: [String]()) {
            $0.append(contentsOf:$1.products)
        }.reduce(into: Set<String>(), {
            $0.formUnion(tree.dependencies(for: $1))
        }).array
        
        self.actualDependencyMapping?.append(DependencyMapping(target: " * ", products: allProducts))

        let resolved = ResolvedManifest(from: self, dependenciesTree: tree)
        try resolved.write(to: self.resolvedManifestPath)
        self.resolvedManifest = resolved
        
        return oldDependencyMapping.diff(with: resolved.actualDependencyMapping, against: tree)
    }
    
    public func embedDependencyProjectIfNeeded(confirm: () -> Bool?) throws {
        guard self.embedDependencyProject else {
            LoggerManager.logDebug("Dependency project shouldn't be embedded.")
            return
        }
        let dest = try Project(projectPath: self.targetProjectPath)
        let source = try Project(projectPath: self.dependencyProjectPath)
        
        try dest.add(subproject: source)
        if dest.isDirty && confirm() ?? true {
            try dest.save()
        }
    }
    
    public static func create(inRoot root: Path, forProject targetProjectName: String, forceOverwrite force: Bool, ommitExamples flag: Bool) throws {
        let targetProjectPath = root + targetProjectName
        let manifestPath = root + manifestFileName
        
        LoggerManager.logDebug("Checking if manifest file already exists.")
        if force { LoggerManager.logDebug("You forced it it will work anyway.") }
        
        guard !manifestPath.exists || force else {
            LoggerManager.logDebug("It is, it will fail.")
            throw XSPMError.manifestAlreadyExists
        }
        
        var content: String
        if !flag {
            LoggerManager.logDebug("Generating content with examples.")
            content = """
            /* XSPM Manifest */
            
            - project: "\(targetProjectName)"
            // - dependencies: "Dependencies"
            // - embed: true
            // - toolsVersion: 5.0
            
            /* Dependencies example */
            // + "https://glitab.org/worldglobcorp/superia.git" ["IAMaster", "Chess"] 1.0.3
            // + "https://guthib.com/Aplpe/Xode.git" ["IDEKit", "InterfaceBuilderKit"] >= 10.1.0
            // + "git@glitab.com/Gogle/SearchEngine.git" "SearchKit" [1.0.0, 3.0.1]
            
            /* Dependency mappings example */
            // # "\(targetProjectName.split(separator: ".").first ?? "Target")" ["IDEKit", "SearchKit"]
            """
        } else {
            LoggerManager.logDebug("Generating content without examples.")
            content = """
            /* XSPM Manifest */
            
            - project: "\(targetProjectName)"
            // - dependencies: "Dependencies"
            // - embed: true"
            // - toolsVersion: 5.0
            """
        }
        
        LoggerManager.logDebug("Getting target project's targets...")
        let targetProject = try Project(projectPath: targetProjectPath)
        LoggerManager.logDebug("Targets acquired.")
        let autorizedChars = CharacterSet.alphanumerics + "!@#$%&*()_-+=.?><"
        
        content.lineFeed()
        targetProject.nativeTargetNames.forEach {
            LoggerManager.logDebug("Creating dependency mapping line for \($0)...")
            let targetName = autorizedChars.isSuperset(of: CharacterSet(charactersIn: $0)) ? $0 : "\"\($0)\""
            content.appendLine("# \(targetName): []")
        }
        
        LoggerManager.logDebug("Trying to write manifest.xmnfst...")
        try manifestPath.write(content)
        LoggerManager.logDebug("Succes. Wasn't that hard.")
    }
    
    public func calculateDifferencesWithResolved() throws -> ManifestComparisonResult {
        if self.resolvedManifest == nil { try self.readResolved() }
        return self.compare(with: self.resolvedManifest!)
    }
    
    public func assert(is status: IntegrationStatus) throws {
        let actualStatus = self.status
        guard status == actualStatus else {
            switch actualStatus {
            case .unresolved:
                throw XSPMError.unresolvedManifest
            case .problematic:
                throw XSPMError.ambiguousState
            case .ok:
                throw XSPMError.alreadyInitiated
            }
        }
    }
    
    private func readResolved() throws {
        do {
            self.resolvedManifest = try JSONDecoder().decode(ResolvedManifest.self, from: try self.resolvedManifestPath.read())
        } catch {
            throw XSPMError.cannotReadResolved
        }
    }
}

extension Manifest: ComparableManifest {
    typealias Compared = ResolvedManifest
    
    func compare(with rhs: ResolvedManifest) -> ManifestComparisonResult {
        LoggerManager.logDebug("Starting manifest vs resolved manifest comparison.")
        return self.content.compare(with: rhs.content)
    }
}
