//
//  AbstractLogger.swift
//  xspmkit
//

public protocol LoggerProtocol: AnyObject {
    
    /// Logs a message.
    ///
    /// - Parameter msg: The message.
    func logMessage(_ msg: String)
    
    /// Logs an information.
    ///
    /// - Parameter nfo: The infomation
    func logInfo(_ nfo: String)
    
    /// Logs a debug string.
    ///
    /// - Parameter dbg: The debug string.
    func logDebug(_ dbg: String)
    
    /// Logs an error.
    ///
    /// - Parameter err: The error.
    func logError(_ err: String)
}

