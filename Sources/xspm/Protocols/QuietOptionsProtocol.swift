//
//  QuietOptionsProtocol.swift
//  xspm
//


import Foundation
import Commandant

protocol QuietOptionsProtocol {
    var quiet: Bool { get }
    static var quietSwitch: Switch { get }
    
    func adaptQuietness()
}

extension QuietOptionsProtocol {
    static var quietSwitch: Switch {
        return Switch(flag: "q", key: "quiet", usage: "xspm does the work quietly")
    }
    
    func adaptQuietness() {
        CommonOutput.quiet = self.quiet
    }
}
