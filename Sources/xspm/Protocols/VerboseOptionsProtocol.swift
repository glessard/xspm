//
//  VerboseOptionsProtocol.swift
//  xspm
//

import Foundation
import Commandant
import xspmkit

protocol VerboseOptionsProtocol {
    var verbose: Bool { get }
    static var verboseSwitch: Switch { get }
    
    func adaptVerbosity()
}

extension VerboseOptionsProtocol {
    static var verboseSwitch: Switch {
        return Switch(flag: "v", key: "verbose", usage: "xpsm tells you everything")
    }
    
    func adaptVerbosity() {
        CommonOutput.verbose = self.verbose
        LoggerManager.set(logLevel: self.verbose ? .debug : .info)
    }
}
