//
//  CommonOutput.swift
//  xspm
//

import Foundation
import Rainbow
import xspmkit

class CommonOutput {
    static let loggerProxy = LoggerProxy(shared)
    static let shared = CommonOutput(verbose: true)
    static var verbose: Bool {
        get {
            return CommonOutput.default.verbose
        }
        set(flag) {
            CommonOutput.default.verbose = flag
        }
    }
    static var quiet: Bool {
        get {
            return CommonOutput.default.quiet
        }
        set(flag) {
            CommonOutput.default.quiet = flag
        }
    }
    
    private static let `default` = CommonOutput()
    
    var quiet: Bool = false
    var verbose: Bool = false
    
    private init(verbose: Bool = false) {
        self.verbose = verbose
    }
    
    class func say(that str: String) {
        CommonOutput.default.say(that: str)
    }
    
    fileprivate func say(that str: String) {
        guard !self.quiet else { return }
        
        print("\(str)")
    }
    
    class func inform(that str: String) {
        CommonOutput.default.inform(that: str)
    }
    
    fileprivate func inform(that str: String) {
        guard !self.quiet else { return }
        print("ℹ \(str)".cyan)
    }
    
    class func ask(if str: String) {
        CommonOutput.default.ask(if: str)
    }
    
    fileprivate func ask(if str: String) {
        print("⚑ \(str)".yellow, terminator: "")
    }
    
    class func warn(that str: String) {
        CommonOutput.default.warn(that: str)
    }
    
    fileprivate func warn(that str: String) {
        guard !self.quiet else { return }
        print("⚠︎ \(str)".yellow)
    }
    
    class func debug(that str: String) {
        CommonOutput.default.debug(that: str)
    }
    
    fileprivate func debug(that str: String) {
        guard !self.quiet && self.verbose else { return }
        print("⚙︎ \(str)".yellow)
    }
    
    class func sayJobsDone() {
        CommonOutput.default.sayJobsDone()
    }
    
    fileprivate func sayJobsDone() {
        guard !self.quiet else { return }
        print("\n✨🌟   \("Jobs done".yellow)   🌟✨")
    }
    
    class func sayItsOkay(because str: String) {
        CommonOutput.default.sayItsOkay(because: str)
    }
    
    fileprivate func sayItsOkay(because str: String) {
        guard !self.quiet else { return }
        print("✔︎ \(str)".green)
    }
    
    class func sayItsNotOkay(because str: String? = nil) {
        CommonOutput.default.sayItsNotOkay(because: str)
    }
    
    fileprivate func sayItsNotOkay(because str: String? = nil) {
        if let reason = str {
            print("🤒 \(reason.red.bold)")
        } else {
            print("🤒 \("An unknown seriously-serious thing went wrong.".red.bold)")
        }
    }
    
    class func feed() {
        CommonOutput.default.feed()
    }
    
    fileprivate func feed() {
        guard !self.quiet else { return }
        print()
    }
}

class LoggerProxy {
    private unowned let object: CommonOutput
    
    fileprivate init(_ obj: CommonOutput) {
        self.object = obj
    }
}

extension LoggerProxy: LoggerProtocol {
    func logMessage(_ msg: String) { self.object.say(that: msg) }
    func logInfo(_ nfo: String) { self.object.inform(that: nfo) }
    func logDebug(_ dbg: String) { self.object.debug(that: dbg) }
    func logError(_ err: String) { self.object.sayItsNotOkay(because: err) }
}
