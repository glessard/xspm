//
//  File.swift
//  xspm
//
//

import Foundation
import Commandant
import Curry
import Result
import PathKit
import xspmkit

struct StatusCommandOptions: OptionsProtocol, VerboseOptionsProtocol {
    typealias ClientError = XSPMError
    
    let verbose: Bool
    
    static func evaluate(_ m: CommandMode) -> Result<StatusCommandOptions, CommandantError<XSPMError>> {
        return curry(self.init)
            <*> m <| verboseSwitch
    }
}

struct StatusCommand: XSPMCommandProtocol {
    typealias Options = StatusCommandOptions
    
    var verb: String = "status"
    var function: String = "🚦 - Show xspm integration status"
    
    func run(_ options: StatusCommandOptions) throws {
        let root = Path.current
        
        do {
            CommonOutput.say(that: "\n🚦 - Showing xspm integration status... \n".bold)
            
            let manifest = try Manifest.read(inRoot: root)
            
            switch manifest.status {
            case .ok:
                CommonOutput.inform(that: "Everything appears to be ok.")
            case .problematic:
                CommonOutput.inform(that: "There seems to be a issue with the integration.")
            case .unresolved:
                CommonOutput.inform(that: "The integration is still un-initialized.")
            }
        } catch let error as XSPMError {
            guard case .noManifest = error else { throw error }
            CommonOutput.inform(that: "The integration is not bootstrapped, there's no manifest.")
        }
    }
}
