//
//  CleanCommand.swift
//  xspm
//

import Foundation
import Commandant
import Curry
import Result
import xspmkit
import PathKit

struct CleanCommandOptions: OptionsProtocol, VerboseOptionsProtocol, QuietOptionsProtocol {
    typealias ClientError = XSPMError
    
    let keepManifest: Bool
    let verbose: Bool
    let quiet: Bool
    
    static func evaluate(_ m: CommandMode) -> Result<CleanCommandOptions, CommandantError<ClientError>> {
        return curry(self.init)
            <*> m <| Switch(flag: "k", key: "keep", usage: "Keep manifest.xmnfst")
            <*> m <| verboseSwitch
            <*> m <| quietSwitch
    }
}

struct CleanCommand: XSPMCommandProtocol {
    typealias Options = CleanCommandOptions
    
    var verb: String = "clean"
    var function: String = "🛁 - Delete dependencies"
    
    func run(_ options: CleanCommandOptions) throws {
        let root = Path.current
        
        CommonOutput.say(that: "\n🛁 Cleaning project from dependencies...\n".bold)
        try cleanFiles(in: root, keepManifest: options.keepManifest)
        CommonOutput.sayItsOkay(because: "Cleaned !")
        
        CommonOutput.sayJobsDone()
    }
}
