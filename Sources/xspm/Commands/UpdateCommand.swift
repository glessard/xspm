//
//  UpdateCommand.swift
//  xspm
//

import Foundation
import Commandant
import Curry
import Result
import xspmkit
import PathKit

struct UpdateCommandOptions: OptionsProtocol, VerboseOptionsProtocol, QuietOptionsProtocol, SaveLogsOptionsProtocol {
    typealias ClientError = XSPMError
    
    var verbose: Bool
    var quiet: Bool
    var saveLogs: Bool
    
    static func evaluate(_ m: CommandMode) -> Result<UpdateCommandOptions, CommandantError<XSPMError>> {
        return curry(self.init)
            <*> m <| verboseSwitch
            <*> m <| quietSwitch
            <*> m <| saveLogsSwitch
    }
}

struct UpdateCommand: XSPMCommandProtocol {
    typealias Options = UpdateCommandOptions
    
    let verb: String = "update"
    let function: String = "💫 - Update dependencies"
    
    func run(_ options: UpdateCommandOptions) throws {
        let root = Path.current
        let saveLogs = options.saveLogs
        
        CommonOutput.say(that: "\n💫 Updating dependencies... \n".bold)
        
        let manifest = try Manifest.read(inRoot: root)
        try manifest.assert(is: .ok)
        
        let package = Package(root: root, manifest: manifest)
        
        let diff = try manifest.calculateDifferencesWithResolved()
        
        if diff.requirePackageRewrite {
            CommonOutput.inform(that: "Updating Package.swift...")
            try package.write()
            CommonOutput.sayItsOkay(because: "Package.swift updated.")
        }
        
        CommonOutput.inform(that: "Updating dependencies...")
        try package.update(saveLog: options.saveLogs)
        CommonOutput.sayItsOkay(because: "Dependencies updated.")
        
        CommonOutput.inform(that: "Updating dependency project...")
        try package.generateXcodeproj(saveLog: saveLogs)
        CommonOutput.sayItsOkay(because: "Dependency project updated.")
        
        try manifest.embedDependencyProjectIfNeeded(confirm: confirmProjectEmbedding)
        
        CommonOutput.feed()
        
        CommonOutput.inform(that: "Resolving targets dependendencies...")
        let tree = try package.getDependenciesTree(saveLog: saveLogs)
        let result = try manifest.resolve(against: tree)
        CommonOutput.sayItsOkay(because: "Target dependencies resolved.")
        
        if !result.isUseless {
            CommonOutput.inform(that: "Here is the target mapping and recommanded project configuration:")
            displayDiffResult(of: result, against: try manifest.getResolvedDependenciesMapping())
        }
        
        CommonOutput.sayJobsDone()
    }
}
