//
//  File.swift
//  xspm
//

import Foundation
import Commandant
import Curry
import Result
import xspmkit

struct VersionCommand: XSPMCommandProtocol {
    static let XSPM_VERSION: String = "0.6.5"
    
    typealias Options = NoOptions<XSPMError>
    
    var verb: String = "version"
    var function: String = "🔩 - Show xspm version"
    
    func run(_ options: NoOptions<XSPMError>) throws {
        CommonOutput.inform(that: VersionCommand.XSPM_VERSION)
    }
}
