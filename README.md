# xspm
xspm is a tool that helps you integrate Swift packages in Xcode. You can add it to any project old or new.  
xspm integrates Swift packages in **macOS, iOS, tvOS & wathOS** projects, even with mixed targets. This means that the same dependency can link to a macOS target and an iOS one in your projects.


xspm is still in beta so first please read the [disclaimer](#disclaimer).

- [**Quickstart**](#quickstart)
- [**Installation**](#installation)
- [**How it works**](#how-it-works)
- [**Commands**](#commands)
- [**The manifest file**](#the-manifest-file)
- [**Contributing**](#contributing)
- [**Building tools leveraging xspm**](#building-tools-leveraging-xspm)
- [**FAQ**](#faq)
- [**Disclaimer**](#disclaimer)
- [**License**](#license)

## Quickstart
1. Open a new Terminal window
2. Navigate to the target project root
3. Enter the command `$ xspm bootstrap`
4. Edit the freshly created `manifest.xmnfst` file
5. Close the target project 
6. In the terminal enter `$ xspm init`
7. Open the target project and see that a *Dependencies* subproject has been added.
8. Link the dependencies at will.

## Installation 
For now you can only build xspm from source. You must have the latest Xcode release (10.2) installed.

Open a terminal and run

```text
sh -c "$(curl -fsSL https://gitlab.com/Pyroh/xspm/raw/master/install.sh)"
```

Voilà !
Note that there's no actual method to update xspm, just reinstall it when you hear of a new version. It will eventually be available through brew some day.

## How it works
The *trick* used to integrate Swift packages in Xcode is well known. It consists of creating a new package of type *framework*, adding the packages you want to integrate and generate an Xcode project. This project is then added to the target project and the frameworks coming out of the embedded dependency project can be linked at will.

xspm simply automates this process and makes your life easier.

1. create a manifest file at the root of the target project
2. edit the manifest file by adding dependencies 
3. optionally add some target mappings
5. link dependencies inside of the target project

When an update is needed or if the target project needs a new dependency xpsm will handle it flawlessly. You just need to add new dependencies to the manifest file and use `$ xspm update`. 

There's a sample project that shows how to integrate xspm with an Xcode project [in this repository](https://gitlab.com/Pyroh/xspmsample).

## Commands 
### `bootstrap`   🏁 - Create the manifest file
You must run this command from the target project's root. The `manifest.xmnfst` is created and ready for you to edit.  
You can use this command without any parameter, it will then find the *xcodeproj* file in the current directory and treat it as the target project. If there's more than one *xcodeproj* file in the current directory you must specify which one you want to use.  
#### Parameters
```text
[<name>]
	Name of the target project

--force|-f
	Overwrite existing manifest file

--no-example|-i
	Don't add example to the manifest file

--verbose|-v
	xpsm tells you everything

--quiet|-q
	xspm does the work quietly
```
#### Typical result
```text
🏁 Bootstraping.

ℹ Creating manifest.xmnfst file...
✔︎ manifest.xmnfst file created.

✨🌟   Jobs done   🌟✨
```

### `init`        🚀 - Initialize dependencies
Once you've set the dependencies and the mappings for your project you use this command to actually fetch the related packages and create the dependency project. xspm can also embed the dependency project in the target project.
#### Parameters
```text
--no-ignore|-g
	Don't add dependencies folder to gitignore

--force|-f
	Reinit dependencies with the same name if any

--verbose|-v
	xpsm tells you everything

--quiet|-q
	xspm does the work quietly

--save-logs|-l
	Save all spm logs
```
#### Typical result
```text
🚀 Initializing dependencies...

ℹ Creating Package.swift...
✔︎ Package.swift created.
ℹ Resolving dependencies...
✔︎ Dependencies resolved.
ℹ Creating dependency project...
✔︎ Dependency project created.
ℹ Adding dependencie folder to .gitignore
✔︎ Folder already added.
✔︎ Dependencies folder added to .gitignore.

ℹ Resolving targets dependendencies...
✔︎ Target dependencies resolved.
ℹ Here is the recommanded project configuration:
ℹ 2 new targets has been added:
	 -  * : Sylex, CoreGeometry, Roaster
	 - transformer: Sylex, CoreGeometry, Roaster

✨🌟   Jobs done   🌟✨
```

As you can see xspm informs you of the (strongly) suggested target mapping. The `*` is a special target you don't have choice to be aware of. This special target tells you what dependencies you need to actually link if you want to link every dependencies to one of your targets. Yes, dependencies may have dependencies themselves and now you know which ones.

### `update`      💫 - Update dependencies
Updates the already initialized dependencies and reflects changes in the manifest file. 
#### Parameters
```text
--verbose|-v
	xpsm tells you everything

--quiet|-q
	xspm does the work quietly

--save-logs|-l
	Save all spm logs
```
#### Typical result
```text
💫 Updating dependencies...

ℹ Updating dependencies...
✔︎ Dependencies updated.
ℹ Updating dependency project...
✔︎ Dependency project updated.

ℹ Resolving targets dependendencies...
✔︎ Target dependencies resolved.
ℹ Here is the recommanded project configuration:
ℹ 2 targets has been left unchanged:
	 -  * : Sylex, CoreGeometry, Roaster
	 - transformer: Sylex, CoreGeometry, Roaster

✨🌟   Jobs done   🌟✨
```

### `show`        🌳 - Show resolved dependencies tree
Dumps the dependency tree and shows target mapping.
#### Parameters
```text
--verbose|-v
	xpsm tells you everything
```
#### Typical result
```text
🌳 Showing dependencies tree...

✔︎ Dependencies tree:
 ‣ Sylex
 ‣ CoreGeometry
 ‣ Roaster
   ‣ CoreGeometry
   ‣ Sylex

✔︎ Per-target dependencies mapping:
transformer: Sylex, CoreGeometry, Roaster
 * : Sylex, CoreGeometry, Roaster

✨🌟   Jobs done   🌟✨
```

### `status`      🚦 - Show xspm integration status
Not so interesting, exist mainly for debug purpose.
#### Parameters
```text
--verbose|-v
	xpsm tells you everything
```
#### Typical result
```text
🚦 - Showing xspm integration status...

ℹ Everything appears to be ok.
```

### `clean`       🛁 - Delete dependencies
Sometimes you may want to stop using xspm and use something else or nothing else. To do so you can delete the *Dependencies* folder (you may have set its name to something else), the manifest file and remove any reference of the dependency project from the target one.  
Or you can use this command that deletes the *Dependencies* folder and the manifest file. To clean the target project you're on your own, sorry.
#### parameters
```text
--keep|-k
	Keep manifest.xmnfst

--verbose|-v
	xpsm tells you everything

--quiet|-q
	xspm does the work quietly
```
#### Typical result
```text
🛁 Cleaning project from dependencies...

✔︎ Cleaned !

✨🌟   Jobs done   🌟✨
```

### `version`     🔩 - Show xspm version
You'll have to guess this one.

### `help`        🤔 - Display general or command-specific help
For command-specific help use `$ xspm help <command>`.

## The manifest file
This file is always named *manifest.xmnfst*. Its content is designed around the *one line, one directive* concept. There's three kinds of lines:

1. the *property* ones
2. the *dependency* ones
3. the *mapping* ones

You don't really have to sort lines by kind in the file. xspm will read the lines one by one and do something with it. If it does make sense of course. 

### *Property* line

`-` `property name` `:` `value`

Any line that describes a property value starts with a `-` followed by the property name. Then comes the colon `:` and finally the property value that can be of numeric, textual or boolean type.  
For example a mandatory property is the `project` one. It's the target project name and is generally at the top of the file (remember, you can do whatever you want). For example :
`- project: "MyAwesomeProject.xcodeproj"`

#### List of properties

| Property | Type | Description | Default value |
|:--|:--|:--|:--|
| project | `String` | The filename of the target project | *None, is mandatory* |
| dependencies | `String` | The name of the folder were xspm puts the dependencies. Also the name of the generated dependency project | *"Dependencies"* |
| embed | `Bool` | Wether or not the dependency project should be embedded in the target project | *true* |
| toolsVersion | `String` | The tools-version property of the dependency package | *None, is inferred by the current SPM version* |
| xcconfig | `String` | Name of the .xcconfig file | *None, don't use config file* |

Every other property name will result in an error. No, there's not a single property which type is numeric but it could.

> **Note for xcconfig**: The file must be placed alongside the manifest, in the same folder.

### *Dependency* line

`+` `URL` `Product list` `version`

Any line that describes a dependency starts with a `+` followed  by the package's URL (quoted, local relative URLs also work 🥳) and the product's names you want to use from this package. This can be a unique product name or an array of product names. Finally comes the version of the package you want to use **leaving it blank will assume `.branch("master")`**. For example :  
`+ "https://github.com/Alamofire/Alamofire.git" Alamofire >= 5.0.0-beta.3` <- *Uses the product `Alamofire` from the Alamofire package in version `5.0.0-beta.3` and later.*  
`+ "https://github.com/apple/swift-package-manager.git" [SwiftPM, SPMUtility]` <- *Uses the products SwiftPM and SPMUtility from the package swift-package-manager directly from the `master` branch*

> Note that the product names are not quoted. You can quote them if you want.

#### Version range
The Swift Package Manager has its own method to set the version range. xspm's one sticks to the same principle with a different syntax :

| Swift PM | xspm | Version range |
|:--|:--|:--|
| `from: "1.2.0"` | `1.2.0` | 1.0.0 ..< 2.0.0 |
| `.upToNextMajor(from: "1.5.8")` | `>= 1.5.8` | 1.5.8 ..< 2.0.0 |
| `.upToNextMinor(from: "1.5.8")` | `>~ 1.5.8` | 1.5.8 ..< 1.6.0 |
| `.exact("1.5.8")` | `== 1.5.8` | 1.5.8 |
| `"1.2.3"..<"1.2.6"` | `[1.2.3 1.2.6[` | 1.2.3 ..< 1.2.6 |
| `"1.2.3"..."1.2.8"` | `[1.2.3 1.2.8]` | 1.2.3 ... 1.2.8 |
| `.branch("develop")` | `"develop"` | Branch *develop* |
| `.revision("e74b...a021")` | `<e74b...a021>` | Revision *e74b...a021* |

### *Mapping* line

`#` `Target name` `:` `Product list`

Any line that describes a dependency mapping starts with a `#` followed by the target name and a colon `:`. Finally comes the product list it can be either a single product name or an array of them. For example:

`# SuperProject: [Alamofire, SwiftPM]`

xspm won't actually link these dependancies with the target inside of you target project. What it does is simply telling you at the end of an `init` or `update` command which dependencies should be linked to which targets.

## Building tools leveraging xspm
xspm is the combination of to modules : the core `xspmkit` and the CLI `xspm`. This means that you can use `xspmkit` in your project for example to built a GUI version of xspm.

The code is not documented at all but object and method names are self-explanatory (at least I think so). The API may be considered unstable as small changes are foreseen. Mainly additive though. xspm's source code may help you understand how it works.

### Using `xspmkit` in your project
#### Through xspm
Simply add this line to your *manifest.xmnfst* file and update your target mapping :
```text
+ "https://gitlab.com/Pyroh/xspm.git" xspmkit
```
Then update the dependencies with `$ xspm update`.

#### Through the Swift Package Manager
Add this line to the dependencies :
```text
.package(url: "https://gitlab.com/Pyroh/xspm.git", .branch("master"))
```

And add `xspmkit` in the `dependencies` property of the target.

## Contributing 
You may want to contribute to xspm. Or you may just want to take a look at the code. For this you must generate the xspm's *xcodeproj* file.

Open a terminal and navigate to the folder where you cloned xspm (refer to the [Installation section](#installation) if you need help). Then type :

```text
$ make dev
```

It will generate the *xspm.xcodeproj* file. You're all set.

## FAQ
- *Why GitLab instead of GitHub*

	Why not ?
	
- *Why doesn't the `clean` command also clean the target project ?*

	Because manipulating an *xcodeproj* file in order to add things is safer than manipulating it to delete stuff. I mean it's easy to know where to put items but deleting them is a lot more tricky. It's easy to delete the wrong item or more than we should and end up messing the whole *xcodeproj* file.

- *I'm writing non-sense in the manifest file and it still does something instead giving me an error. Why ?*

	Because the manifest parser is still widely untested. You can help writing the tests 😀.
	
- *Your tool is aware of the project targets during the bootstrap process why doesn't it add ready-to-map targets in the manifest file ?*

	~~Hey it's actually a good idea ! Might think of it for the next release.~~ Update xspm it shipped with version `0.6.5`.
	
- *What are these packages you use in your examples ?*

	Some lib of mine I use for my in-house projects. [CoreGeometry](https://gitlab.com/Pyroh/CoreGeometry) is public and the code is well documented. I still need to generate and host the documentation somewhere.
	 
- *Why don't you use libSwiftPM instead of relying on the `swift package` command ?*

	From the Swift Package Manager repository on [GitHub](https://github.com/apple/swift-package-manager/blob/master/Documentation/libSwiftPM.md): 
	> **NOTE: The libSwiftPM API is currently *unstable* and may change at any time**.
	
## Disclaimer
xspm is provided as-is. It is still considered in beta. So if xspm messes up your entire project I can't be held responsible of anything.  
Of course you're not dumb and you won't use a highly untested piece of software in a context that may trash an important project. Even if you end up doing such a thing be sure to `$ git commit` first.

## License
MIT License

Copyright (c) 2019 Pierre Tacchi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
